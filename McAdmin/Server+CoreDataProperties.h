//
//  Server+CoreDataProperties.h
//  McAdmin
//
//  Created by Andrew Querol on 7/27/15.
//  Copyright © 2015 Andrew Querol. All rights reserved.
//
//  Delete this file and regenerate it using "Create NSManagedObject Subclass…"
//  to keep your implementation up to date with your model.
//

#import "Server.h"

NS_ASSUME_NONNULL_BEGIN

@interface Server (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *direct;
@property (nullable, nonatomic, retain) NSString *direct_uri;
@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *name;

@end

NS_ASSUME_NONNULL_END
