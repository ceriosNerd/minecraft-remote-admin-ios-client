//
//  ConsoleCell.m
//  McAdmin
//
//  Created by Andrew Querol on 7/27/15.
//  Copyright © 2015 Andrew Querol. All rights reserved.
//

#import "ConsoleCell.h"

@implementation ConsoleCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
