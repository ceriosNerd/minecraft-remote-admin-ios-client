//
//  CoreDataHelper.m
//  McAdmin
//
//  Created by Andrew Querol on 7/25/15.
//  Copyright © 2015 Andrew Querol. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "CoreDataHelper.h"
#import "AppDelegate.h"

@interface CoreDataHelper ()
@property (nonatomic, readonly) NSManagedObjectContext       *context;
@property (nonatomic, readonly) NSManagedObjectModel         *model;
@property (nonatomic, readonly) NSPersistentStoreCoordinator *coordinator;
@property (nonatomic, readonly) NSPersistentStore            *store;

+ (NSURL *)applicationStoresDirectory;
- (void)initStore;
@end

@implementation CoreDataHelper

@synthesize model;
@synthesize coordinator;
@synthesize context;
@synthesize store;

- (id)init {
    self = [super init];
    if (self) {
        coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [context setPersistentStoreCoordinator:coordinator];
    }
    return self;
}

+ (NSURL *)applicationStoresDirectory {
    NSURL *storesDirectory = [[NSURL fileURLWithPath:[AppDelegate applicationDocumentsDirectory]] URLByAppendingPathComponent:@"Stores"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[storesDirectory path]]) {
        NSError *error = nil;
        if (![fileManager createDirectoryAtURL:storesDirectory withIntermediateDirectories:YES attributes:nil error:&error]) {
            NSLog(@"FAILED to create Stores directory: %@", error);
        }
#ifdef DEBUG
        else {
            NSLog(@"Successfully created Stores directory");
        }
#endif
    }
    return storesDirectory;
}

+ (NSURL *)storeURL {
    return [[CoreDataHelper applicationStoresDirectory] URLByAppendingPathComponent:(NSString *)storeFilename];
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel {
    if (model == nil) {
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"SavedServers" withExtension:@"momd"];
        model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    }
    
    return model;
}

- (void)initStore {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSError *error = nil;
        store = [coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:[CoreDataHelper storeURL] options:nil error:&error];
        if (!store) {
            NSLog(@"Failed to add store. Error: %@", error);
            UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:[error localizedDescription] message:[error localizedRecoverySuggestion] preferredStyle:UIAlertControllerStyleAlert];
            [errorAlert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * __nonnull action) {
                abort();
            }]];
        }
#ifdef DEBUG
        else {
            NSLog(@"Successfully added store: %@", store);
        }
#endif
    });
}

- (void)setupCoreData {
    [self initStore];
}

- (NSManagedObjectContext *)getContext {
    return context;
}

- (void)saveContext {
    if ([context hasChanges]) {
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Failed to save changes to the store: %@", error);
        }
    }
}

@end
