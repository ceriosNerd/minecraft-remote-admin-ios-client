//
//  main.m
//  McAdmin
//
//  Created by Andrew Querol on 7/13/15.
//  Copyright © 2015 Andrew Querol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
