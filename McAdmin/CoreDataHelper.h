//
//  CoreDataHelper.h
//  McAdmin
//

#import <Foundation/Foundation.h>

@class NSManagedObjectContext;

static const NSString *storeFilename = @"MCAdmin.sqlite";

@interface CoreDataHelper :NSObject

- (void)setupCoreData;
- (void)saveContext;
- (NSManagedObjectContext *) getContext;
- (NSManagedObjectModel *) managedObjectModel;

+ (NSURL *)storeURL;
@end