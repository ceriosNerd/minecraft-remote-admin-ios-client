//
//  Server.h
//  McAdmin
//
//  Created by Andrew Querol on 7/27/15.
//  Copyright © 2015 Andrew Querol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@class KeychainItemWrapper;

@interface Server : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
@property (nonatomic) boolean_t directBoolean;
@property (nonatomic) NSString *decryptedKey;
@property (nonatomic) KeychainItemWrapper *keychainItem;

@end

NS_ASSUME_NONNULL_END

#import "Server+CoreDataProperties.h"
