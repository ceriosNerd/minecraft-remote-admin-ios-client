//
//  DetailViewController.h
//  McAdmin
//
//  Created by Andrew Querol on 7/13/15.
//  Copyright © 2015 Andrew Querol. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Server;

@interface DetailViewController : UIViewController <NSStreamDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) Server *serverObject;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *commandTextField;

- (IBAction)onSendCommandPress:(UIButton *)sender;

@end

