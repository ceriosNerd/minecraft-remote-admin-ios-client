//
//  ConsoleCell.h
//  McAdmin
//
//  Created by Andrew Querol on 7/27/15.
//  Copyright © 2015 Andrew Querol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConsoleCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@end
