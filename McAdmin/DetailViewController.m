//
//  DetailViewController.m
//  McAdmin
//
//  Created by Andrew Querol on 7/13/15.
//  Copyright © 2015 Andrew Querol. All rights reserved.
//

#import "DetailViewController.h"
#import "Server.h"
#import "ConsoleCell.h"

@interface DetailViewController ()

typedef enum {
    DEAD,
    ALIVE,
    UNKNOWN
} CONN_STATUS;

- (void) tryConnect;

@property NSMutableArray<NSString *> *consoleLines;

@property NSInputStream *readStream;
@property NSOutputStream *writeStream;
@property NSMutableData *readBuffer;
@property NSNumber *dataSize;
@property NSMutableArray<NSString *> *pendingMessages;
@property dispatch_source_t timer;
@property dispatch_source_t raceConditionMessageChecker;
@property volatile NSDate *lastSeenServer;
@property volatile CONN_STATUS connectionStatus;
@end

@implementation DetailViewController

@synthesize readStream;
@synthesize writeStream;
@synthesize readBuffer;
@synthesize timer;
@synthesize raceConditionMessageChecker;
@synthesize pendingMessages;
@synthesize connectionStatus;
@synthesize consoleLines;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Managing the detail item

- (void)setServer:(Server *)serverObject {
    if (self.serverObject != serverObject) {
        self.serverObject = serverObject;
        // TODO: Load server address or ask for it
        // TODO: Load the admin key if saved
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.serverObject) {
        [self setTitle:self.serverObject.name];
    } else {
        [self dismissViewControllerAnimated:true completion:nil];
    }
    
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
}

#pragma mark - Table View Stuff

- (NSInteger)numberOfSectionsInTableView:(nonnull UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return consoleLines.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ConsoleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConsoleLineCell" forIndexPath:indexPath];
    [cell.label setText:[consoleLines objectAtIndex:indexPath.row]];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [consoleLines removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 22;
}

#pragma mark - Server Logic

// Helper method to create a GCD dispatch block with a timer
dispatch_source_t CreateDispatchTimer(double interval, dispatch_queue_t queue, dispatch_block_t block) {
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    if (timer) {
        dispatch_source_set_timer(timer, dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), interval * NSEC_PER_SEC, (1ull * NSEC_PER_SEC) / 10);
        dispatch_source_set_event_handler(timer, block);
        dispatch_resume(timer);
    }
    return timer;
}

- (IBAction)onServerButtonPress:(UIBarButtonItem *)sender {
    [self tryConnect];
}

- (void)tryConnect {
    self.connectionStatus = UNKNOWN;
    // TODO: This is a temporary call, this allows me to test functionality without implementing the admin key storage/entry system
    
    CFReadStreamRef cfRead;
    CFWriteStreamRef cfWrite;
    CFStreamCreatePairWithSocketToCFHost(kCFAllocatorDefault, CFHostCreateWithName(kCFAllocatorDefault, (CFStringRef)@"localhost"), 1337, &cfRead, &cfWrite);
    self.readStream = (__bridge NSInputStream *) cfRead;
    self.writeStream = (__bridge NSOutputStream *) cfWrite;
    
    // Allow self signed certs
    // TODO make this not the default, try to connect with trusted only and then warn the user and ask if they want to continue.
    CFReadStreamSetProperty((CFReadStreamRef)[self readStream], kCFStreamPropertySSLSettings, CFBridgingRetain([NSMutableDictionary dictionaryWithObject:(NSNumber *)kCFBooleanFalse forKey:(NSString *)kCFStreamSSLValidatesCertificateChain]));
    [[self readStream] setProperty:[NSDictionary dictionaryWithObjectsAndKeys: (NSNumber *)kCFBooleanFalse, kCFStreamSSLValidatesCertificateChain, nil] forKey:(NSString *)kCFStreamPropertySSLSettings];
    CFReadStreamSetProperty((CFReadStreamRef)[self readStream], kCFStreamPropertySSLSettings, CFBridgingRetain([NSMutableDictionary dictionaryWithObject:(NSNumber *)kCFBooleanFalse forKey:(NSString *)kCFStreamSSLValidatesCertificateChain]));
    [[self readStream] setProperty:[NSDictionary dictionaryWithObjectsAndKeys: (NSNumber *)kCFBooleanFalse, kCFStreamSSLValidatesCertificateChain, nil] forKey:(NSString *)kCFStreamPropertySSLSettings];
    
    [readStream setDelegate:self];
    [writeStream setDelegate:self];
    [readStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [writeStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [readStream open];
    [writeStream open];
}

- (void)sendMessage:(NSString *) message {
    // We add the newline character to the message automaticly for use as a message delimiter. Remove any present already to prevent packet confusion.
    message = [message stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\n"]];
    if (message != NULL && message.length > 0 && self.connectionStatus != DEAD) {
        message = [message stringByAppendingString:@"\n"]; // Add the message delimiter
        if (writeStream.hasSpaceAvailable) {
            if ([pendingMessages count] > 0) {
                // To keep order correct we need to send any pending first and then put the new requrest into pending
                NSData *data = [[pendingMessages firstObject] dataUsingEncoding:NSUTF8StringEncoding];
                [writeStream write:[data bytes] maxLength:data.length];
                [pendingMessages removeObject:[pendingMessages firstObject]];
                // No return so we can fall down to adding to the message queue
            } else {
                /* The callback thread has stoped sending event messages since we didn't write the last time it sent
                * NSStreamEventHasSpaceAvailable, in this case it is safe to write directly
                */
                NSData *data = [message dataUsingEncoding:NSUTF8StringEncoding];
                [writeStream write:[data bytes] maxLength:[data length]];
                return; // We sent the message, no need to add it to the pending queue
            }
        }
        // We can only send messages when the OS tells us we can in the callback
        [pendingMessages addObject:message];
    }
}

- (void)readMessage:(NSString *) message {
    // Read a full message
    NSArray<NSString*> *messageParts = [[message stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\n"]] componentsSeparatedByString:@":"];
    if (messageParts.count < 1) {
        return;
    }
    
    NSString *command = messageParts[0];
    if ([command hasPrefix:@"PING"]) {
        [self sendMessage:@"ACK"];
    } else if ([command hasPrefix:@"ERR"]) {
        self.connectionStatus = DEAD;
        [self closeOrFinalizeConnection];
    } else if ([command hasPrefix:@"ADMINKEY?"]) {
        [self sendMessage:[self.serverObject decryptedKey]];
    } else if ([command hasPrefix:@"REAUTH"]) {
        NSString *reauthMessage = [NSString stringWithFormat:@"REAUTH:%@\r%@", [[[UIDevice currentDevice] identifierForVendor] UUIDString], [self.serverObject decryptedKey]];
        [self sendMessage:reauthMessage];
    } else if ([command hasPrefix:@"CONSOLE"]) {
        if (messageParts.count < 2)
            return;
        if (consoleLines == nil) {
            consoleLines = [NSMutableArray array];
        }
        NSString *decoded = [[NSString alloc] initWithData:[[NSData alloc] initWithBase64EncodedString:messageParts[1] options:0] encoding:NSUTF8StringEncoding];
        if ([decoded stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0) {
            return;
        }
        NSLog(@"%@", command);

        
        [self.tableView beginUpdates];
        [consoleLines addObject:decoded];
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:[consoleLines indexOfObject:decoded] inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
    // The remote relay has contacted us, reset the last seen timestamp
    self.lastSeenServer = [NSDate date];
}

- (void)updateContentInset {
    NSInteger numRows=[self.tableView numberOfRowsInSection:0];
    CGFloat contentInsetTop=self.tableView.bounds.size.height;
    for (int i=0;i<numRows;i++) {
        contentInsetTop-=[[self.tableView delegate] tableView:self.tableView heightForRowAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
        if (contentInsetTop<=0) {
            contentInsetTop=0;
            break;
        }
    }
    self.tableView.contentInset = UIEdgeInsetsMake(contentInsetTop, 0, 0, 0);
}

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode {
    switch (eventCode) {
        case NSStreamEventHasBytesAvailable: {
            if (![aStream isKindOfClass:[NSInputStream class]]) {
                return; // I don't know how anything other than an input stream can have bytes available but I'll check just in case
            }
            uint8_t *buffer;
            NSUInteger length;

            BOOL freeBuffer = NO;
            // The stream has data. Try to get its internal buffer instead of creating one
            if(![readStream getBuffer:&buffer length:&length]) {
                // The stream couldn't provide its internal buffer. We have to make one ourselves
                buffer = malloc(1024 * sizeof(uint8_t));
                freeBuffer = YES;
                NSInteger result = [readStream read:buffer maxLength:1024];
                if(result < 0) {
                    NSLog(@"Unknown error occured when reading.\nIt is possible that you got disconnected from the remote server.");
                    [self closeOrFinalizeConnection];
                    return;
                }
                length = result;
            }
            [readBuffer appendBytes:buffer length:length];
            while (readBuffer.length > 0) {
                NSRange newlineLocation = [readBuffer rangeOfData:[@"\n" dataUsingEncoding:NSUTF8StringEncoding] options:0 range:NSMakeRange(0, [readBuffer length])];
                if (newlineLocation.location == NSNotFound) {
                    break;
                }
                newlineLocation = NSMakeRange(0, newlineLocation.location + 1);
                NSString *message = [[NSString alloc] initWithData:[readBuffer subdataWithRange:newlineLocation] encoding:NSUTF8StringEncoding];
                [self readMessage:message];
                [readBuffer replaceBytesInRange:newlineLocation withBytes:nil length:0];
            }
        } break;
        case NSStreamEventOpenCompleted: {
            // We only care about the opening of the write stream in this part of the code
            if (![aStream isEqual:writeStream]) {
                return;
            }
            
            NSLog(@"Connected to server list");
            self.connectionStatus = ALIVE;
            
            // Create the read buffer
            if (readBuffer == nil) {
                readBuffer = [[NSMutableData alloc] init];
            }
            if (pendingMessages == nil) {
                pendingMessages = [[NSMutableArray alloc] init];
            }
            self.lastSeenServer = [NSDate date];
            [self sendMessage:[NSString stringWithFormat:@"CLIENT:iOS-%@", [[[UIDevice currentDevice] identifierForVendor] UUIDString]]];
            self.timer = CreateDispatchTimer(30 /* Minutes */ * 60 /* Seconds */, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                if (self.connectionStatus != ALIVE) {
                    self.connectionStatus = DEAD;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self closeOrFinalizeConnection];
                        dispatch_source_cancel(self.timer);
                    });
                    return;
                }
                
                if ([self.lastSeenServer timeIntervalSinceDate:[NSDate date]] > 30 /* Minutes */ * 60 /* Seconds */) {
                    // Set the status to unknown, we failed a heartbeat somewhere
                    self.connectionStatus = UNKNOWN;
                    // Also try to ping the server to restore normal operations
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self sendMessage:@"PING"];
                    });
                }
            });
            // Set to one minute. Hopefully this doesn't have an impact on energy
            self.raceConditionMessageChecker = CreateDispatchTimer(60, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                // Check if the connection closed since last time we got called and stop this queue if true
                if (self.connectionStatus == DEAD) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        dispatch_source_cancel(self.raceConditionMessageChecker);
                    });
                    return;
                }
                // Check for messages that might be able to stall when the callbacks stop due to inactivity and calling sendMessage when that happens.
                if (pendingMessages.count > 0) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // Send one packet, if there are any more packets they'll get sent due to the wake up of the callback messages
                        NSData *data = [[pendingMessages firstObject] dataUsingEncoding:NSUTF8StringEncoding];
                        [writeStream write:[data bytes] maxLength:data.length];
                        [pendingMessages removeObject:[pendingMessages firstObject]];
                    });
                }
            });
        } break;
        case NSStreamEventHasSpaceAvailable: {
            if (pendingMessages.count > 0) {
                NSData *data = [[pendingMessages firstObject] dataUsingEncoding:NSUTF8StringEncoding];
                [writeStream write:[data bytes] maxLength:data.length];
                [pendingMessages removeObject:[pendingMessages firstObject]];
            }
        } break;
        case NSStreamEventErrorOccurred: {
            NSError *error = [self.readStream streamError];
            NSLog(@"Error %@ / %zd", [error domain], (ssize_t) [error code]);
        } // No break to allow dropdown to disconnect
        case NSStreamEventEndEncountered: {
            // Remote server ended connection
            NSLog(@"Disconnected from server");
            [self closeOrFinalizeConnection];
            return;
        } break;
        default: {
            // If we receive a unknown event code log it
            NSLog(@"Received unknown event code from NSStream: %lul", (unsigned long)eventCode);
        } break;
    }
}

- (void) closeOrFinalizeConnection {
    if (writeStream.streamStatus != NSStreamStatusClosed) {
        if (writeStream.streamStatus == NSStreamStatusOpen) {
            // Kindly let the other end know that we are disconnecting if the stream is still valid
            [self sendMessage:@"BYE!"];
        }
        [writeStream close];
    }
    // Close the streams if needed
    if (readStream.streamStatus != NSStreamStatusClosed) {
        [readStream close];
    }
    self.connectionStatus = DEAD;
    
    // TODO Seuge back to main server selection page after presenting a promt allowing the user to reconnect if desired
}

- (void)dealloc {
    // Be nice to the OS and close our streams if still open
    [readStream close];
    [writeStream close];

    // Release the bridged CF classes
    if (readStream != nil) {
        CFRelease((__bridge CFTypeRef)(readStream));
    }
    if (writeStream != nil) {
        CFRelease((__bridge CFTypeRef)(writeStream));
    }
}

- (IBAction)onSendCommandPress:(UIButton *)sender {
    if (self.connectionStatus != DEAD) {
        NSString *base64EncodedCommand = [[self.commandTextField.text dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];
        [self sendMessage:[@"CMD:" stringByAppendingString:base64EncodedCommand]];
        self.commandTextField.text = nil;
    } else {
        UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Not Conencted" message:@"You are not connected to this server, do you want to try to connect?" preferredStyle:UIAlertControllerStyleActionSheet];
        [errorAlert addAction:[UIAlertAction actionWithTitle:@"Connect" style:UIAlertActionStyleDefault handler:^(UIAlertAction * __nonnull action) {
            [self tryConnect];
        }]];
        [errorAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:errorAlert animated:true completion:nil];
    }
}

@end
