//
//  AddServerPopoverController.m
//  McAdmin
//
//  Created by Andrew Querol on 7/26/15.
//  Copyright © 2015 Andrew Querol. All rights reserved.
//

#import "AddServerPopoverController.h"
#import "Server.h"
#import "AppDelegate.h"
#import "CoreDataHelper.h"

@interface AddServerPopoverController ()
@property CGSize originalSize;

- (void) resetPopover;
- (void) disableDirectConnectAddress;
- (void) enableDirectConenctAddress;
@end

@implementation AddServerPopoverController
@synthesize originalSize;
@synthesize objectToEdit;
@synthesize tableViewRef;

- (instancetype)initWithNibName:(nullable NSString *)nibNameOrNil bundle:(nullable NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.modalPresentationStyle = UIModalPresentationPopover;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self resetPopover];
}

- (void) resetPopover {
    [self.connectionTypeSwitch setOn:false animated:false];
    [self disableDirectConnectAddress];
}

- (void) enableDirectConenctAddress {
    [self.directConnectAddressTextField setEnabled:true];
    [self setPreferredContentSize:originalSize];
}

- (void) disableDirectConnectAddress {

    [self setPreferredContentSize:CGSizeMake(originalSize.width, self.directConnectAddressTextField.frame.origin.y)];
    [self.directConnectAddressTextField setEnabled:false];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    originalSize = self.view.bounds.size;
    [self resetPopover];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)directConnectSwitchToggled:(UISwitch *)sender {
    if ([sender isOn]) {
        [self enableDirectConenctAddress];
    } else {
        [self disableDirectConnectAddress];
    }
}

- (IBAction)onInfoPress:(UIButton *)sender {
}

- (IBAction)onSavePress:(UIBarButtonItem *)sender {
    if (self.objectToEdit == nil) {
        objectToEdit = [NSEntityDescription insertNewObjectForEntityForName:@"Server" inManagedObjectContext:[[AppDelegate getCoreDataHelper] getContext]];
    }
    [objectToEdit setName:self.displayNameTextField.text];
    [objectToEdit setDirectBoolean:[self.connectionTypeSwitch isOn]];
    if ([objectToEdit directBoolean]) {
        [objectToEdit setDirect_uri:self.directConnectAddressTextField.text];
    }
    [objectToEdit setDecryptedKey:self.adminKeyTextField.text];
    [[AppDelegate getCoreDataHelper] saveContext];
    [tableViewRef insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self dismissViewControllerAnimated:true completion:nil];
}

@end
