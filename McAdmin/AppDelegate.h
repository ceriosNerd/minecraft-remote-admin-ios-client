//
//  AppDelegate.h
//  McAdmin
//
//  Created by Andrew Querol on 7/13/15.
//  Copyright © 2015 Andrew Querol. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CoreDataHelper;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (NSString *)applicationDocumentsDirectory;
+ (CoreDataHelper *) getCoreDataHelper;

@end

