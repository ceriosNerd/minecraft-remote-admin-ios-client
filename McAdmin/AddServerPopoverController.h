//
//  AddServerPopoverController.h
//  McAdmin
//
//  Created by Andrew Querol on 7/26/15.
//  Copyright © 2015 Andrew Querol. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Server;

@interface AddServerPopoverController : UIViewController

@property (weak) UITableView *tableViewRef;
@property Server *objectToEdit;

#pragma mark - UI Properties and Actions
@property (weak, nonatomic) IBOutlet UISwitch *connectionTypeSwitch;
@property (weak, nonatomic) IBOutlet UITextField *displayNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *directConnectAddressTextField;
@property (weak, nonatomic) IBOutlet UITextField *adminKeyTextField;

- (IBAction)directConnectSwitchToggled:(UISwitch *)sender;
- (IBAction)onInfoPress:(UIButton *)sender;
- (IBAction)onSavePress:(UIBarButtonItem *)sender;
@end
