//
//  Server.m
//  McAdmin
//
//  Created by Andrew Querol on 7/27/15.
//  Copyright © 2015 Andrew Querol. All rights reserved.
//

#import "Server.h"
#import "KeychainItemWrapper.h"

@implementation Server
@synthesize keychainItem;

- (void) setDirectBoolean:(boolean_t) isDirect {
    [self willChangeValueForKey:@"direct"];
    [self setDirect:[NSNumber numberWithBool:isDirect]];
    [self didChangeValueForKey:@"direct"];
}

- (boolean_t) directBoolean {
    [self willAccessValueForKey:@"serverId"];
    NSNumber *tmpValue = [self direct];
    [self didAccessValueForKey:@"serverId"];
    return (tmpValue != nil) ? [tmpValue boolValue] : false;
}

- (NSString * __nonnull)decryptedKey {
    if (keychainItem == nil) {
        keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:[self.id stringValue] accessGroup:nil];
    }
    return [keychainItem objectForKey:(id)kSecValueData];
}

- (void)setDecryptedKey:(NSString * __nonnull)decryptedKey {
    if (keychainItem == nil) {
        keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:[self.id stringValue] accessGroup:nil];
    }
    [keychainItem setObject:decryptedKey forKey:(id)kSecValueData];
}

@end
